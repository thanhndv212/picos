.. |docs| image:: https://img.shields.io/website.svg?down_color=red&down_message=offline&label=docs&logo=read-the-docs&logoColor=white&up_color=brightgreen&up_message=online&url=https%3A%2F%2Fpicos-api.gitlab.io%2Fpicos
  :alt: Documentation
  :target: https://picos-api.gitlab.io/picos/

.. |gitlab| image:: https://img.shields.io/badge/GitLab-source-blue?logo=gitlab&logoColor=white
  :alt: GitLab
  :target: https://gitlab.com/picos-api/picos

.. |pypi| image:: https://img.shields.io/pypi/v/picos?label=PyPI&logo=pypi&logoColor=white
  :alt: PyPI
  :target: https://pypi.org/project/PICOS/

.. |anaconda| image:: https://img.shields.io/conda/vn/picos/picos?label=Anaconda&logo=anaconda&logoColor=white
  :alt: Anaconda
  :target: https://anaconda.org/picos/picos

.. |aur| image:: https://img.shields.io/aur/version/python-picos?label=AUR&logo=arch-linux&logoColor=white
  :alt: AUR
  :target: https://aur.archlinux.org/packages/?SeB=n&K=python-picos

.. |freenode| image:: https://img.shields.io/badge/freenode-%23picos-green.svg
  :alt: Freenode
  :target: https://webchat.freenode.net/?nick=picosuser&channels=%23picos

.. |cov| image:: https://img.shields.io/gitlab/coverage/picos-api/picos/master
  :alt: Coverage
  :target: coverage/

.. |license| image:: https://img.shields.io/badge/copying-GPL--3.0-brightgreen
  :alt: License
  :target: https://choosealicense.com/licenses/gpl-3.0/